# inmerso

### Languages/Technologies

![Python](https://img.shields.io/badge/-Python-000?&logo=Python)
![C](https://img.shields.io/badge/-C-000?&logo=C)
![C++](https://img.shields.io/badge/-C++-000?&logo=c%2b%2b)
![SQL](https://img.shields.io/badge/-SQL-000?&logo=MySQL)
![VHDL](https://img.shields.io/badge/-VHDL-000)

![Linux](https://img.shields.io/badge/-Linux-000?&logo=Linux)
![ESP32](https://img.shields.io/badge/-esp32-000)
![Tofino](https://img.shields.io/badge/-Tofino-000)
